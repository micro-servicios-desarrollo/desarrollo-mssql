<?php

$router->get('/backupmssql', 'GeneracionBackupController@index');
$router->get('/backupmssql/{id}', 'GeneracionBackupController@indexyear');
$router->get('/backupmssql/{id}/month/{idm}', 'GeneracionBackupController@indexmonth');
$router->get('/backupmssqldetail/', 'GeneracionBackupController@detail');
$router->get('/backupmssqldetail/{id}', 'GeneracionBackupController@detailyear');
$router->get('/backupmssqldetail/{id}/month/{idm}', 'GeneracionBackupController@detailmonth');
